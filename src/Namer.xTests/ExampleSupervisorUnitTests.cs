﻿using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Namer.DataLayer.DataContext;
using Namer.DataLayer.Repositories;
using Namer.Domain.Impl.Supervisor;
using Namer.Domain.Model;
using Namer.Domain.Repository;
using Namer.Domain.Supervisor;
using Namer.Domain.ViewModel;
using NamerApi.Utils;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace NamerApi.Tests
{
    public class ExampleSupervisorTests : IDisposable
    {
        private IExampleRepository _exampleRepository;
        private IExampleSupervisor _exampleSupervisor;
        private DataBaseContext _dataBaseContext;

        public ExampleSupervisorTests()
        {
            AutoMapperConfig.Initialize();
            var options = new DbContextOptionsBuilder<DataBaseContext>().UseInMemoryDatabase(databaseName: "HorizonNamer").Options;
            _dataBaseContext = new DataBaseContext(options);
            var m1 = new MyModel() { Id = 1, Name = "test1" };
            var m2 = new MyModel() { Id = 2, Name = "test2" };
            var m3 = new MyModel() { Id = 3, Name = "test3" };
            _dataBaseContext.MyModels.Add(m1);
            _dataBaseContext.MyModels.Add(m2);
            _dataBaseContext.MyModels.Add(m3);
             _dataBaseContext.SaveChanges();
            _exampleRepository = new ExampleRepository(_dataBaseContext);
            _exampleSupervisor = new ExampleSupervisor(_exampleRepository);
        }

        [Fact]
        public async Task GetOne_MyModel()
        {
            var viewModel = await _exampleSupervisor.GetSingleAsync(1);
            Assert.NotNull(viewModel);
            viewModel.Name.Should().Be("test1");
        }

        [Fact]
        public async Task GetAll_MyModel()
        {
            var viewModels = await _exampleSupervisor.GetAllAsync();
            Assert.NotNull(viewModels);
            var results = viewModels.ToList();
            results.Count.Should().Be(3);
            results[2].Name.Should().Be("test3");

        }

        [Fact]
        public async Task Delete_MyModel()
        {
            var result = await _exampleSupervisor.DeleteAsync(1);
            result.Should().BeGreaterThan(0);
            var models = await _exampleSupervisor.GetAllAsync();
            var results = models.ToList();
            results.Count.Should().Be(2);
        }

        [Fact]
        public async Task Delete_MyModelInvalid()
        {
            var result = await _exampleSupervisor.DeleteAsync(0);
            result.Should().Be(-1);
        }

        public void Dispose()
        {
            _dataBaseContext.Database.EnsureDeleted();
        }
    }
}
