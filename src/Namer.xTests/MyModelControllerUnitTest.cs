﻿using AutoMapper;
using FluentAssertions;
using HorizonNamer.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Namer.Domain.Model;
using Namer.Domain.Repository;
using Namer.Domain.Supervisor;
using Namer.Domain.ViewModel;
using Namer.xTests.Utils;
using NamerApi.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Namer.xTests
{
    public class MyModelControllerUnitTests : IDisposable
    {
        private Moq.Mock<IExampleSupervisor> _exampleSupervisor;
        private MyModelController _controller;

        public MyModelControllerUnitTests()
        {
            AutoMapper.ServiceCollectionExtensions.UseStaticRegistration = false;
            AutoMapperConfig.Initialize();
            _exampleSupervisor = new Moq.Mock<IExampleSupervisor>();
            var repo = new Moq.Mock<IExampleRepository>();
            var logger = new Moq.Mock<ILogger<MyModelController>>();
            var svcProvider = new Moq.Mock<IServiceProvider>();
            var m1 = new MyModel() { Id = 1, Name = "test1" };
            var m2 = new MyModel() { Id = 2, Name = "test2" };
            var m3 = new MyModel() { Id = 3, Name = "test3" };
            var data = new List<MyModelViewModel>();
            data.Add(Mapper.Map<MyModelViewModel>(m1));
            data.Add(Mapper.Map<MyModelViewModel>(m2));
            data.Add(Mapper.Map<MyModelViewModel>(m3));
            IEnumerable<MyModelViewModel> data1 = data;
            _exampleSupervisor.Setup(x => x.GetAllAsync(default(CancellationToken))).Callback(() => { }).Returns(() => Task.FromResult(data1));
            _exampleSupervisor.Setup(x => x.GetSingleAsync(1, default(CancellationToken))).Callback(() => { }).Returns(() => Task.FromResult(data[0]));
            _controller = new MyModelController(repo.Object, logger.Object, svcProvider.Object, _exampleSupervisor.Object);
        }


        [Fact]
        public async Task MyModel_Get_All()
        {
            // Act
            var result = await _controller.Get();
            // Assert
            var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
            var actual = okResult.Value.Should().BeAssignableTo<IEnumerable<MyModelViewModel>>().Subject;
            actual.Count().Should().Be(3);
        }

        [Fact]
        public async Task MyModel_GetOne()
        {
            var result = await _controller.Get(1);
            // Assert
            var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
            var actual = okResult.Value.Should().BeAssignableTo<MyModelViewModel>().Subject;
            actual.Name.Should().Be("test1");
        }

        public void Dispose()
        {
            //AutoMapper.Mapper.Reset();
        }
    }
}
