using FluentAssertions;
using Namer.Domain.Model;
using Namer.Domain.ViewModel;
using Namer.xTests.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Namer.xTests
{
    public class MyModelControllerIntegrationTests : IClassFixture<TestFixture<HorizonNamer.Startup>>
    {
        private readonly HttpClient _client;

        public MyModelControllerIntegrationTests(TestFixture<HorizonNamer.Startup> fixture)
        {
            AutoMapper.ServiceCollectionExtensions.UseStaticRegistration = false;
            _client = fixture.Client;
        }

        [Fact]
        public async Task Integration_MyModel_Get_All()
        {
            // Act
            var response = await _client.GetAsync("/api/MyModel");
            // Assert
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            var models = JsonConvert.DeserializeObject<IEnumerable<MyModelViewModel>>(responseString);
            models.Count().Should().BeGreaterOrEqualTo(0);
        }

        [Fact]
        public async Task Integration_MyModel_Get_Specific()
        {
            // Act
            var response = await _client.GetAsync("/api/MyModel/1");

            // Assert
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            var model = JsonConvert.DeserializeObject<MyModelViewModel>(responseString);
            model.Id.Should().Be(1);
        }

        [Fact]
        public async Task Integration_MyModel_Post_Specific()
        {
            // Arrange
            var modelToAdd = new MyModelViewModel
            { Name = "rick" };
            var content = JsonConvert.SerializeObject(modelToAdd);
            var stringContent = new StringContent(content, Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync("/api/MyModel", stringContent);

            // Assert
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            var person = JsonConvert.DeserializeObject<MyModel>(responseString);
            person.Id.Should().BeGreaterOrEqualTo(1);
        }
    }
}
