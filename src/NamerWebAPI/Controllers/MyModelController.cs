﻿using System;
using System.Linq;
using System.Net;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NamerApi.Utils;
using Namer.Common;
using Namer.Domain.Repository;
using Namer.Domain.ViewModel;
using Namer.Domain.Model;
using Namer.Domain.Supervisor;
using System.Threading.Tasks;

namespace HorizonNamer.Controllers
{
    [Route("api/[controller]")]
    public class MyModelController : Controller
    {
        //private readonly IExampleRepository _exampleRepository;
        private readonly IExampleSupervisor _exampleSupervisor;
        private readonly ILogger _logger;
        private readonly IServiceProvider _serviceProvider;
        public MyModelController(IExampleRepository exampleRepository, ILogger<MyModelController> logger, IServiceProvider provider, IExampleSupervisor supervisor)
        {
            //_exampleRepository = exampleRepository;
            _logger = logger;
            _serviceProvider = provider;
            _exampleSupervisor = supervisor;
        }

        // GET: api/mymodel
        [HttpGet("", Name = "GetAll")]
        public async Task<IActionResult> Get()
        {
            _logger.LogInformation(LoggingEvents.GetItem, "Getting all items");
            //IExampleRepository repository = HttpContext.RequestServices.GetService<IExampleRepository>();
            //var repo = _serviceProvider.GetService<IExampleRepository>();
            //var repo = AppDependencyResolver.Current.GetService<IExampleRepository>();
            try
            {
                return Ok(await _exampleSupervisor.GetAllAsync());
            }
            catch (Exception exception)
            {
                //logg exception or do anything with it
                _logger.LogError(exception.Message);
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        // GET api/values/5
        [HttpGet("{id}", Name = "GetSingle")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var myModel = await _exampleSupervisor.GetSingleAsync(id);

                if (myModel == null)
                {
                    return NotFound();
                }

                return Ok(myModel);
            }
            catch (Exception exception)
            {
                //Do something with the exception
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        // POST api/values
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]MyModelViewModel viewModel)
        {
            try
            {
                if (viewModel == null)
                {
                    return BadRequest();
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var item = await _exampleSupervisor.SaveAsync(viewModel);

                if (item.Id > 0)
                {
                    return CreatedAtRoute("GetSingle", new { controller = "MyModel", id = item.Id }, item);
                }

                return BadRequest();
            }
            catch (Exception exception)
            {
                //Do something with the exception
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody]MyModelViewModel viewModel)
        {
            try
            {
                if (viewModel == null)
                {
                    return BadRequest();
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var singleById = await _exampleSupervisor.PutAsync(id, viewModel);

                if (singleById.Id == -1)
                {
                    return NotFound();
                }
                else if (singleById.Id > 0)
                {
                    return Ok(singleById);
                }

                return BadRequest();
            }
            catch (Exception exception)
            {
                //Do something with the exception
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var saveResult = await _exampleSupervisor.DeleteAsync(id);

                if (saveResult == -1)
                {
                    return NotFound();
                }
                else if (saveResult > 0)
                {
                    return NoContent();
                }

                return BadRequest();
            }
            catch (Exception exception)
            {
                //Do something with the exception
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }
    }
}
