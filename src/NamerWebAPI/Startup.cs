﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.IO;
using HorizonNamer.Swagger;
using NamerApi.Utils;
using Autofac;
using Middleware;
using System.Net;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Namer.Common;
using Namer.DataLayer.DataContext;
using Namer.Domain.Model;
using Namer.Domain.ViewModel;
using VMD.RESTApiResponseWrapper.Core.Extensions;

namespace HorizonNamer
{
    public class Startup
    {
        private readonly IHostingEnvironment _hostingEnv;
        public Startup(IHostingEnvironment env, IConfiguration configuration)
        {
            _hostingEnv = env;
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            var configurationSection = Configuration.GetSection("ConnectionStrings:DefaultConnection");
            services.AddCors(o => o.AddPolicy("corsPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));
            services.AddDbContext<DataBaseContext>(options => options.UseSqlServer(configurationSection.Value));
            // Add framework services.
            services.AddElm();
            services.AddMvc(config =>
            {
                config.Filters.Add(typeof(CustomExceptionFilter));
            }).AddJsonOptions(options => {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });

            // Adds a default in-memory implementation of IDistributedCache.
            services.AddDistributedMemoryCache();

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(100);
                options.Cookie.HttpOnly = true;
            });
            //services.AddScoped<IExampleRepository, ExampleRepository>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new Info
                    {
                        Version = "v1",
                        Title = "Horizon Namer API",
                        Description = "Horizon Namer API",
                        TermsOfService = "......"
                    }
                );

                c.ParameterFilter<TestParameterFilter>();

                c.OperationFilter<AssignOperationVendorExtensions>();
                c.OperationFilter<FormDataOperationFilter>();

                c.DescribeAllEnumsAsStrings();

                c.SchemaFilter<ExamplesSchemaFilter>();

                //c.DescribeAllParametersInCamelCase();
            });

            if (_hostingEnv.IsDevelopment())
            {
                services.ConfigureSwaggerGen(c =>
                {
                    c.DescribeAllEnumsAsStrings();
                    //var xmlCommentsPath = Path.Combine(System.AppContext.BaseDirectory, "Basic.xml");
                    //c.IncludeXmlComments(xmlCommentsPath, true);
                });
            }
            var serviceProvider = services.AddAutofacProvider();
            return serviceProvider;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            AppDependencyResolver.Init(app.ApplicationServices);
            app.UseCors("corsPolicy");
            AutoMapperConfig.Initialize();
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            app.UseElmPage();
            app.UseElmCapture();

            app.UseAPIResponseWrapperMiddleware();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseHttpStatusCodeExceptionMiddleware();
            }
            else
            {
                app.UseHttpStatusCodeExceptionMiddleware();
                app.UseExceptionHandler(
                     options => {
                         options.Run(
                         async context =>
                         {
                             context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                             context.Response.ContentType = "application/json";
                             var ex = context.Features.Get<IExceptionHandlerFeature>();
                             if (ex != null)
                             {
                                 var err = ex.Error.Message + " " + ex.Error.StackTrace;
                                 var result = JsonConvert.SerializeObject(new { error = err });
                                 await context.Response.WriteAsync(result).ConfigureAwait(false);
                             }
                         });
                     }
                    );
            }

            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseSession();
            app.UseMvc();

            app.UseSwagger(c =>
            {
                c.PreSerializeFilters.Add((swagger, httpReq) => swagger.Host = httpReq.Host.Value);
            });

            app.UseSwaggerUI(c =>
            {
                c.RoutePrefix = ""; // serve the UI at root
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "V1 Docs");

                c.ShowExtensions();
            });
        }
    }
}
