﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NamerApi.Utils
{
    public static class IoCExtensions
    {
        public static IServiceProvider AddAutofacProvider(this IServiceCollection services, Action<ContainerBuilder> builderCallback = null)
        {
            // Instantiate the Autofac builder
            var builder = new ContainerBuilder();

            // If there is a callback use it for registrations
            builderCallback?.Invoke(builder);

            // Populate the Autofac container with services
            builder.Populate(services);
            builder.RegisterModule(new Namer.Domain.AutofacM.SupervisorModule());
            builder.RegisterModule(new Namer.DataLayer.AutofacM.RepositoryModule());
            // Create a new container with component registrations
            var container = builder.Build();
            var provider = new AutofacServiceProvider(container);

            // When application stops then dispose container
            container.Resolve<IApplicationLifetime>()
                .ApplicationStopped.Register(() => container.Dispose());

            // Return the provider
            return provider;
        }
    }
}
