﻿using Namer.Domain.Model;
using Namer.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NamerApi.Utils
{
    public static class AutoMapperConfig
    {
        private static object _thisLock = new object();
        private static bool _initialized = false;
        // Centralize automapper initialize
        public static void Initialize()
        {
            // This will ensure one thread can access to this static initialize call
            // and ensure the mapper is reseted before initialized
            lock (_thisLock)
            {
                if (!_initialized)
                {
                    AutoMapper.Mapper.Reset();
                    AutoMapper.Mapper.Initialize(config =>
                    {
                        config.CreateMap<MyModel, MyModelViewModel>().ReverseMap();
                    });
                    _initialized = true;
                }
            }
        }
    }
}
