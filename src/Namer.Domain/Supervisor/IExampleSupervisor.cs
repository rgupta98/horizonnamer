﻿using Namer.Domain.Model;
using Namer.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Namer.Domain.Supervisor
{
    public interface IExampleSupervisor
    {
        Task<IEnumerable<MyModelViewModel>> GetAllAsync(CancellationToken ct = default(CancellationToken));
        IEnumerable<MyModelViewModel> GetAll();
        MyModelViewModel GetSingle(int id);
        Task<MyModelViewModel> GetSingleAsync(int id, CancellationToken ct = default(CancellationToken));
        MyModel Save(MyModelViewModel model);
        Task<MyModel> SaveAsync(MyModelViewModel viewModel, CancellationToken ct = default(CancellationToken));
        int Delete(int id);
        Task<int> DeleteAsync(int id, CancellationToken ct = default(CancellationToken));
        MyModelViewModel Put(int id, MyModelViewModel viewModel);
        Task<MyModelViewModel> PutAsync(int id, MyModelViewModel viewModel, CancellationToken ct = default(CancellationToken));
    }
}
