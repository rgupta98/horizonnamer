﻿using AutoMapper;
using Namer.Domain.AutofacM;
using Namer.Domain.Model;
using Namer.Domain.Repository;
using Namer.Domain.Supervisor;
using Namer.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Namer.Domain.Impl.Supervisor
{
    public class ExampleSupervisor : IExampleSupervisor
    {
        private readonly IExampleRepository _exampleRepository;
        private readonly IMapper _mapper;
        private readonly IMapper _viewModelMapper;
        public ExampleSupervisor(IExampleRepository repo)
        {
            _exampleRepository = repo;
            _mapper = typeof(MyModel).GetTypeMapper<MyModel>();
            _viewModelMapper = typeof(MyModelViewModel).GetTypeMapper<MyModelViewModel>();
        }

        public int Delete(int id)
        {
            MyModel singleById = _exampleRepository.GetSingle(id);

            if (singleById == null)
            {
                return -1;
            }

            _exampleRepository.Delete(singleById);
            int save = _exampleRepository.Save();
            return save;
        }

        public async Task<int> DeleteAsync(int id, CancellationToken ct = default(CancellationToken))
        {
            MyModel singleById = await _exampleRepository.GetSingleAsync(id);

            if (singleById == null)
            {
                return -1;
            }
            return await _exampleRepository.DeleteAsync(singleById, ct);
        }

        public virtual IEnumerable<MyModelViewModel> GetAll()
        {
            var models = _exampleRepository.GetAll().Select(x => _mapper.Map<MyModelViewModel>(x));
            return models;
        }

        public async Task<IEnumerable<MyModelViewModel>> GetAllAsync(CancellationToken ct = default(CancellationToken))
        {
            var models = await _exampleRepository.GetAllAsync(ct);
            return models.Select(x => _mapper.Map<MyModelViewModel>(x));
        }

        public MyModelViewModel GetSingle(int id)
        {
            MyModel myModel = _exampleRepository.GetSingle(id);

            if (myModel == null)
            {
                return null;
            }
            return _mapper.Map<MyModelViewModel>(myModel);
        }

        public async Task<MyModelViewModel> GetSingleAsync(int id, CancellationToken ct = default(CancellationToken))
        {
            MyModel myModel = await _exampleRepository.GetSingleAsync(id, ct);

            if (myModel == null)
            {
                return null;
            }
            return _mapper.Map<MyModelViewModel>(myModel);
        }

        public MyModelViewModel Put(int id, MyModelViewModel viewModel)
        {
            MyModel singleById = _exampleRepository.GetSingle(id);

            if (singleById == null)
            {
                return new MyModelViewModel() { Id = -1 };
            }

            singleById.Name = viewModel.Name;

            _exampleRepository.Update(singleById);
            int save = _exampleRepository.Save();
            if(save > 0)
            {
                return _mapper.Map<MyModelViewModel>(singleById);
            }
            return new MyModelViewModel();
        }

        public async Task<MyModelViewModel> PutAsync(int id, MyModelViewModel viewModel, CancellationToken ct = default(CancellationToken))
        {
            MyModel singleById = await _exampleRepository.GetSingleAsync(id);

            if (singleById == null)
            {
                return new MyModelViewModel() { Id = -1 };
            }

            singleById.Name = viewModel.Name;

            var save = await _exampleRepository.UpdateAsync(singleById);
            if (save > 0)
            {
                return _mapper.Map<MyModelViewModel>(singleById);
            }
            return new MyModelViewModel();
        }

        public MyModel Save(MyModelViewModel viewModel)
        {
            MyModel item = _viewModelMapper.Map<MyModel>(viewModel);

            _exampleRepository.Add(item);
            int save = _exampleRepository.Save();
            if(save > 0)
                return item;
            return new MyModel();
        }

        public async Task<MyModel> SaveAsync(MyModelViewModel viewModel, CancellationToken ct = default(CancellationToken))
        {
            MyModel item = _viewModelMapper.Map<MyModel>(viewModel);

            var save = await _exampleRepository.AddAsync(item, ct);
            if (save > 0)
                return item;
            return new MyModel();
        }
    }
}
