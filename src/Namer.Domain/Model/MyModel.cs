﻿using Namer.Domain.AutofacM;
using Namer.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Namer.Domain.Model
{
    [MapTarget(typeof(MyModelViewModel))]
    public class MyModel
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
