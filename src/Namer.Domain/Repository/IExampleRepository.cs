﻿using Namer.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Namer.Domain.Repository
{
    public interface IExampleRepository
    {
        Task<IEnumerable<MyModel>> GetAllAsync(CancellationToken ct = default(CancellationToken));
        IEnumerable<MyModel> GetAll();
        MyModel GetSingle(int id);
        Task<MyModel> GetSingleAsync(int id, CancellationToken ct = default(CancellationToken));
        MyModel Add(MyModel toAdd);
        Task<int> AddAsync(MyModel toAdd, CancellationToken ct = default(CancellationToken));
        MyModel Update(MyModel toUpdate);
        Task<int> UpdateAsync(MyModel toUpdate, CancellationToken ct = default(CancellationToken));
        void Delete(MyModel toDelete);
        Task<int> DeleteAsync(MyModel toDelete, CancellationToken ct = default(CancellationToken));
        int Save();
        Task<int> SaveAsync(CancellationToken ct = default(CancellationToken));
    }
}
