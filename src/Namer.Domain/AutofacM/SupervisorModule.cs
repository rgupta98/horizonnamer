﻿using Autofac;
using Namer.Domain.Impl.Supervisor;
using Namer.Domain.Supervisor;
using System;
using System.Collections.Generic;
using System.Text;

namespace Namer.Domain.AutofacM
{
    public class SupervisorModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ExampleSupervisor>().As<IExampleSupervisor>().InstancePerLifetimeScope();
        }
    }
}
