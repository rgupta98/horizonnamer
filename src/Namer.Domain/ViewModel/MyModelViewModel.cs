﻿using Namer.Domain.AutofacM;
using Namer.Domain.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Namer.Domain.ViewModel
{
    [MapTarget(typeof(MyModel))]
    public class MyModelViewModel
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
