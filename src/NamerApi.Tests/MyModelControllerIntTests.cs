﻿using FluentAssertions;
using HorizonNamer;
using HorizonNamer.Controllers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Namer.Domain.Model;
using Namer.Domain.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace NamerApi.Tests
{
    [TestClass]
    public class MyModelControllerIntTests 
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;
        public MyModelControllerIntTests()
        {
            //var startupAssembly = typeof(HorizonNamer.Startup).GetTypeInfo().Assembly;
            //var contentRoot = GetProjectPath("", startupAssembly);
            // Arrange
            //AutoMapper.Mapper.Reset();
            _server = new TestServer(new WebHostBuilder()
             .UseEnvironment("Development")
             .UseContentRoot(System.AppContext.BaseDirectory)
             .ConfigureAppConfiguration((hostContext, config) =>
             {
                 // delete all default configuration providers
                 config.Sources.Clear();
                 config.SetBasePath(System.AppContext.BaseDirectory);
                 config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
                 config.AddJsonFile($"appsettings.{hostContext.HostingEnvironment.EnvironmentName}.json", optional: true);
             })
                .UseStartup<Startup>());
            _client = _server.CreateClient();
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        [TestMethod]
        public async Task Int_MyModel_Get_All()
        {
            // Act
            var response = await _client.GetAsync("/api/MyModel");
            // Assert
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            var models = JsonConvert.DeserializeObject<IEnumerable<MyModelViewModel>>(responseString);
            models.Count().Should().BeGreaterOrEqualTo(0);
        }

        [TestMethod]
        public async Task Int_MyModel_Get_Specific()
        {
            // Act
            var response = await _client.GetAsync("/api/MyModel/1");

            // Assert
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            var model = JsonConvert.DeserializeObject<MyModelViewModel>(responseString);
            model.Id.Should().Be(1);
        }

        [TestMethod]
        public async Task Int_MyModel_Post_Specific()
        {
            // Arrange
            var modelToAdd = new MyModelViewModel
            { Name = "rick" };
            var content = JsonConvert.SerializeObject(modelToAdd);
            var stringContent = new StringContent(content, Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync("/api/MyModel", stringContent);

            // Assert
            response.EnsureSuccessStatusCode();
            var responseString = await response.Content.ReadAsStringAsync();
            var person = JsonConvert.DeserializeObject<MyModel>(responseString);
            person.Id.Should().BeGreaterOrEqualTo(1);
        }

        /// <summary>
        /// Gets the full path to the target project that we wish to test
        /// </summary>
        /// <param name="projectRelativePath">
        /// The parent directory of the target project.
        /// e.g. src, samples, test, or test/Websites
        /// https://docs.microsoft.com/en-us/aspnet/core/mvc/controllers/testing?view=aspnetcore-2.1
        /// </param>
        /// <param name="startupAssembly">The target project's assembly.</param>
        /// <returns>The full path to the target project.</returns>
        private static string GetProjectPath(string projectRelativePath, Assembly startupAssembly)
        {
            // Get name of the target project which we want to test
            var projectName = startupAssembly.GetName().Name;

            // Get currently executing test project path
            var applicationBasePath = System.AppContext.BaseDirectory;

            // Find the path to the target project
            var directoryInfo = new DirectoryInfo(applicationBasePath);
            do
            {
                directoryInfo = directoryInfo.Parent;

                var projectDirectoryInfo = new DirectoryInfo(Path.Combine(directoryInfo.FullName, projectRelativePath));
                if (projectDirectoryInfo.Exists)
                {
                    var projectFileInfo = new FileInfo(Path.Combine(projectDirectoryInfo.FullName, projectName, $"{projectName}.csproj"));
                    if (projectFileInfo.Exists)
                    {
                        return Path.Combine(projectDirectoryInfo.FullName, projectName);
                    }
                }
            }
            while (directoryInfo.Parent != null);

            throw new Exception($"Project root could not be located using the application root {applicationBasePath}.");
        }
    }
}
