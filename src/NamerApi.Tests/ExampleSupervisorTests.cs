using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Namer.DataLayer.DataContext;
using Namer.DataLayer.Repositories;
using Namer.Domain.Impl.Supervisor;
using Namer.Domain.Model;
using Namer.Domain.Repository;
using Namer.Domain.Supervisor;
using Namer.Domain.ViewModel;
using NamerApi.Utils;
using System.Linq;
using System.Threading.Tasks;

namespace NamerApi.Tests
{
    [TestClass]
    public class ExampleSupervisorTests
    {
        private IExampleRepository _exampleRepository;
        private IExampleSupervisor _exampleSupervisor;
        private DataBaseContext _dataBaseContext;

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            AutoMapperConfig.Initialize();
        }

        [TestCleanup]
        public async Task TestCleanup()
        {
            await _dataBaseContext.Database.EnsureDeletedAsync();
        }
        [TestInitialize]
        public async Task Init()
        {
            var options = new DbContextOptionsBuilder<DataBaseContext>().UseInMemoryDatabase(databaseName: "HorizonNamer").Options;
            _dataBaseContext = new DataBaseContext(options);
            var m1 = new MyModel() { Id = 1, Name = "test1" };
            var m2 = new MyModel() { Id = 2, Name = "test2" };
            var m3 = new MyModel() { Id = 3, Name = "test3" };
            _dataBaseContext.MyModels.Add(m1);
            _dataBaseContext.MyModels.Add(m2);
            _dataBaseContext.MyModels.Add(m3);
            await _dataBaseContext.SaveChangesAsync();
            _exampleRepository = new ExampleRepository(_dataBaseContext);
            _exampleSupervisor = new ExampleSupervisor(_exampleRepository);
        }
        [TestMethod]
        public async Task GetOne_MyModel()
        {
            var viewModel = await _exampleSupervisor.GetSingleAsync(1);
            Assert.IsNotNull(viewModel);
            Assert.AreEqual("test1", viewModel.Name);
        }

        [TestMethod]
        public async Task GetAll_MyModel()
        {
            var viewModels = await _exampleSupervisor.GetAllAsync();
            Assert.IsNotNull(viewModels);
            var results = viewModels.ToList();
            Assert.AreEqual(3, results.Count);
            Assert.AreEqual("test3", results[2].Name);
        }

        [TestMethod]
        public async Task Delete_MyModel()
        {
            var result = await _exampleSupervisor.DeleteAsync(1);
            Assert.IsTrue(result > 0);
            var models = await _exampleSupervisor.GetAllAsync();
            var results = models.ToList();
            Assert.AreEqual(2, results.Count);
        }

        [TestMethod]
        public async Task Delete_MyModelInvalid()
        {
            var result = await _exampleSupervisor.DeleteAsync(0);
            Assert.IsTrue(result == -1);
        }
    }
}
