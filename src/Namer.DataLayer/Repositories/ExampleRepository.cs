﻿using Microsoft.EntityFrameworkCore;
using Namer.DataLayer.DataContext;
using Namer.Domain.Model;
using Namer.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Namer.DataLayer.Repositories
{
    public class ExampleRepository : IExampleRepository
    {
        private readonly DataBaseContext _ctx;

        public ExampleRepository(DataBaseContext ctx)
        {
            _ctx = ctx;
        }

        public IEnumerable<MyModel> GetAll()
        {
            return _ctx.MyModels;
        }

        public MyModel GetSingle(int id)
        {
            return _ctx.MyModels.FirstOrDefault(x => x.Id == id);
        }

        public MyModel Add(MyModel toAdd)
        {
            _ctx.MyModels.Add(toAdd);
            return toAdd;
        }

        public MyModel Update(MyModel toUpdate)
        {
            _ctx.MyModels.Update(toUpdate);
            return toUpdate;
        }

        public void Delete(MyModel toDelete)
        {
            _ctx.MyModels.Remove(toDelete);
        }

        public int Save()
        {
            return _ctx.SaveChanges();
        }

        public async Task<IEnumerable<MyModel>> GetAllAsync(CancellationToken ct = default(CancellationToken))
        {
            return await _ctx.MyModels.ToListAsync(ct);
        }

        public async Task<MyModel> GetSingleAsync(int id, CancellationToken ct = default(CancellationToken))
        {
            return await _ctx.MyModels.FirstOrDefaultAsync(x => x.Id == id, ct);
        }

        public async Task<int> DeleteAsync(MyModel toDelete, CancellationToken ct = default(CancellationToken))
        {
            _ctx.MyModels.Remove(toDelete);
            return await _ctx.SaveChangesAsync(ct);
        }

        public async Task<int> SaveAsync(CancellationToken ct = default(CancellationToken))
        {
            return await _ctx.SaveChangesAsync(ct);
        }

        public async Task<int> AddAsync(MyModel toAdd, CancellationToken ct = default(CancellationToken))
        {
            _ctx.MyModels.Add(toAdd);
            return await _ctx.SaveChangesAsync(ct);
        }

        public async Task<int> UpdateAsync(MyModel toUpdate, CancellationToken ct = default(CancellationToken))
        {
            _ctx.MyModels.Update(toUpdate);
            return await _ctx.SaveChangesAsync(ct);
        }
    }
}
