﻿using Autofac;
using Namer.DataLayer.Repositories;
using Namer.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace Namer.DataLayer.AutofacM
{
    public class RepositoryModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ExampleRepository>().As<IExampleRepository>().InstancePerLifetimeScope();
        }
    }
}
