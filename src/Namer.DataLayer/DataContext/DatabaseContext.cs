﻿using Microsoft.EntityFrameworkCore;
using Namer.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Namer.DataLayer.DataContext
{
    public class DataBaseContext : DbContext
    {
        public DbSet<MyModel> MyModels { get; set; }

        public DataBaseContext(DbContextOptions<DataBaseContext> options)
            : base(options)
        { }
    }
}
